<?php
/**
 * 公共控制器
 * ============================================================================
 * 网站地址: http://www.pcfcms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 小潘 <1131680521@qq.com>
 * Date: 2019-12-21
 */
namespace app\common\controller;
use app\BaseController;
use think\facade\Session;
use think\facade\Db;
class Common extends BaseController 
{
    public $theme_style = '';
    public $view_suffix = 'html';
    public $gzpcf = array();
    public $globalTpCache = array();
    
    // 初始化操作
    public function initialize() 
    {
        parent::initialize();
        header("Cache-control: private");// history.back返回后输入框值丢失问题 
        //关闭网站
        if (empty(tpCache('web.web_status'))) {
            die("<!DOCTYPE HTML><html><head><title>网站暂时关闭</title><body><div style='text-align:center;font-size:20px;font-weight:bold;margin:50px 0px;'>网站暂时关闭，维护中……</div></body></html>");
        }
    }

}