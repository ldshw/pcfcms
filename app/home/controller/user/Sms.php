<?php
/**
 * 短信发送
 * ============================================================================
 * 网站地址: http://www.pcfcms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 小潘 <1131680521@qq.com>
 * Date: 2019-12-21
 */
namespace app\home\controller\user;
use app\home\logic\SmsLogic;
class Sms extends Base
{
    public $smsLogic;
    public function initialize(){
        parent::initialize();
        $this->smsLogic = new SmsLogic;
    }

    // 找回密码发送短信
    public function send_sms_retrieve()
	{
        function_exists('set_time_limit') && set_time_limit(5);
		$post = input('param.');
		$email = $post['email'];
		$title = "找回密码";
		$type = 'retrieve_password';
        $data = $this->smtpmailLogic->send_email($email, $title, $type);
        if (1 == $data['code']) {
			$result = ['code' => 1, 'msg' => $data['msg']];
			return $result;
        } else {
			$result = ['code' => 0, 'msg' => $data['msg']];
			return $result;
        }
    }

    // 注册手机发送
    public function send_sms_reg()
	{
        function_exists('set_time_limit') && set_time_limit(5);
		$post = input('param.');
		$phone = $post['phone'];
		$type = 'reg';
        $data = $this->smsLogic->send_sms($phone, $type);
        if (1 == $data['code']) {
			$result = ['code' => 1, 'msg' => $data['msg']];
			return $result;
        } else {
			$result = ['code' => 0, 'msg' => $data['msg']];
			return $result;
        }
    }

	
}