<?php
/**
 * 首页
 * ============================================================================
 * 网站地址: http://www.pcfcms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 小潘 <1131680521@qq.com>
 * Date: 2019-12-21
 */

namespace app\home\controller;
use think\facade\Db;
use think\facade\Request;
use think\facade\Session;
class Index extends Base
{
    public function initialize() {
        parent::initialize();
    }

    public function index(){
        $filename = 'index.html';
        // 生成静态页面代码 - PC端动态访问跳转到静态
        $seo_pseudo = tpCache('seo.seo_pseudo');
        if (file_exists($filename) && 2 == $seo_pseudo) {
            if ((common_ismobile()== "wap" && !file_exists(ROOT_PATH."template/{$this->tpl_theme}/mobile"))) {
                header('HTTP/1.1 301 Moved Permanently');
                header('Location:index.html');
            }
        }
        // 获取当前页面URL
        $result['pageurl'] = 'index';
        $gzpcf = array('field' => $result);
        $this->gzpcf = array_merge($this->gzpcf, $gzpcf);
        $this->assign('gzpcf', $this->gzpcf);
        return $this->fetch(":index");
    }

}