<?php
use samdark\sitemap\Sitemap;

/**
 * 格式化字节大小 
 * @param  number $size      字节数
 * @param  string $delimiter 数字和单位分隔符
 * @return string            格式化后的带单位的大小
 */
function format_bytes($size, $delimiter = '') 
{
    $units = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
    for ($i = 0; $size >= 1024 && $i < 5; $i++) $size /= 1024;
    return round($size, 2) . $delimiter . $units[$i];
}
/**
 * 替换指定的符号
 * @param array $arr 特殊字符的数组集合
 * @param string $replacement 符号
 * @param string $str 字符串
 * @return string
 */
function func_preg_replace($arr = array(), $replacement = ',', $str = '')
{
    if (empty($arr)) {
        $arr = array('，');
    }
    foreach ($arr as $key => $val) {
        $str = preg_replace('/('.$val.')/', $replacement, $str);
    }
    return $str;
}

/**
 * 反格式化字节大小
 * @param  number $size      格式化带单位的大小
 */
function unformat_bytes($formatSize)
{
    $size = 0;
    if (preg_match('/^\d+P/i', $formatSize)) {
        $size = intval($formatSize) * 1024 * 1024 * 1024 * 1024 * 1024;
    } else if (preg_match('/^\d+T/i', $formatSize)) {
        $size = intval($formatSize) * 1024 * 1024 * 1024 * 1024;
    } else if (preg_match('/^\d+G/i', $formatSize)) {
        $size = intval($formatSize) * 1024 * 1024 * 1024;
    } else if (preg_match('/^\d+M/i', $formatSize)) {
        $size = intval($formatSize) * 1024 * 1024;
    } else if (preg_match('/^\d+K/i', $formatSize)) {
        $size = intval($formatSize) * 1024;
    } else if (preg_match('/^\d+B/i', $formatSize)) {
        $size = intval($formatSize);
    }
    $size = strval($size);
    return $size;
}

/**
 * 格式化数据化手机号码
 * @param $mobile
 * @return string
 */
function format_mobile($mobile)
{
    return substr($mobile, 0, 5) . "****" . substr($mobile, 9, 2);
}

//获取当前时间戳
function getTime()
{
    return time();
}

function time_ago($posttime)
{
    //当前时间的时间戳
    $nowtimes = time();
    //相差时间戳
    $counttime = $nowtimes - $posttime;
    //进行时间转换
    if ($counttime <= 60) {
        return '刚刚';
    } else if ($counttime > 60 && $counttime <= 120) {
        return '1分钟前';
    } else if ($counttime > 120 && $counttime <= 180) {
        return '2分钟前';
    } else if ($counttime > 180 && $counttime < 3600) {
        return intval(($counttime / 60)) . '分钟前';
    } else if ($counttime >= 3600 && $counttime < 3600 * 24) {
        return intval(($counttime / 3600)) . '小时前';
    } else if ($counttime >= 3600 * 24 && $counttime < 3600 * 24 * 2) {
        return '昨天';
    } else if ($counttime >= 3600 * 24 * 2 && $counttime < 3600 * 24 * 3) {
        return '前天';
    } else if ($counttime >= 3600 * 24 * 3 && $counttime <= 3600 * 24 * 7) {
        return intval(($counttime / (3600 * 24))) . '天前';
    } else if ($counttime >= 3600 * 24 * 7 && $counttime <= 3600 * 24 * 30) {
        return intval(($counttime / (3600 * 24 * 7))) . '周前';
    } else if ($counttime >= 3600 * 24 * 30 && $counttime <= 3600 * 24 * 365) {
        return intval(($counttime / (3600 * 24 * 30))) . '个月前';
    } else if ($counttime >= 3600 * 24 * 365) {
        return intval(($counttime / (3600 * 24 * 365))) . '年前';
    }
}
//获取客户端浏览器信息 
function GetBrowser()
{
    if(!empty($_SERVER['HTTP_USER_AGENT']))
    {
     $br = $_SERVER['HTTP_USER_AGENT'];
     if (preg_match('/MSIE/i',$br)){
       $br = 'MSIE';
     }
     elseif (preg_match('/Firefox/i',$br)){
       $br = 'Firefox';
     }elseif (preg_match('/Chrome/i',$br)){
       $br = 'Chrome';
     }elseif (preg_match('/Safari/i',$br)){
       $br = 'Safari';
     }elseif (preg_match('/Opera/i',$br)){
       $br = 'Opera';
     }else {
       $br = 'Other';
     }
     return ($br."浏览器");
    }else{
     return "获取浏览器信息失败！";
    }
}
//获取客户端操作系统信息包括win10
function GetOs()
{
    $agent = $_SERVER['HTTP_USER_AGENT'];
    $os = false;
    if (preg_match('/win/i', $agent) && strpos($agent, '95'))
    {
      $os = 'Windows 95';
    }
    else if (preg_match('/win 9x/i', $agent) && strpos($agent, '4.90'))
    {
      $os = 'Windows ME';
    }
    else if (preg_match('/win/i', $agent) && preg_match('/98/i', $agent))
    {
      $os = 'Windows 98';
    }
    else if (preg_match('/win/i', $agent) && preg_match('/nt 6.0/i', $agent))
    {
      $os = 'Windows Vista';
    }
    else if (preg_match('/win/i', $agent) && preg_match('/nt 6.1/i', $agent))
    {
      $os = 'Windows 7';
    }
    else if (preg_match('/win/i', $agent) && preg_match('/nt 6.2/i', $agent))
    {
      $os = 'Windows 8';
    }else if(preg_match('/win/i', $agent) && preg_match('/nt 10.0/i', $agent))
    {
      $os = 'Windows 10';#添加win10判断
    }else if (preg_match('/win/i', $agent) && preg_match('/nt 5.1/i', $agent))
    {
      $os = 'Windows XP';
    }
    else if (preg_match('/win/i', $agent) && preg_match('/nt 5/i', $agent))
    {
      $os = 'Windows 2000';
    }
    else if (preg_match('/win/i', $agent) && preg_match('/nt/i', $agent))
    {
      $os = 'Windows NT';
    }
    else if (preg_match('/win/i', $agent) && preg_match('/32/i', $agent))
    {
      $os = 'Windows 32';
    }
    else if (preg_match('/linux/i', $agent))
    {
      $os = 'Linux';
    }
    else if (preg_match('/unix/i', $agent))
    {
      $os = 'Unix';
    }
    else if (preg_match('/sun/i', $agent) && preg_match('/os/i', $agent))
    {
      $os = 'SunOS';
    }
    else if (preg_match('/ibm/i', $agent) && preg_match('/os/i', $agent))
    {
      $os = 'IBM OS/2';
    }
    else if (preg_match('/Mac/i', $agent) && preg_match('/PC/i', $agent))
    {
      $os = 'Macintosh';
    }
    else if (preg_match('/PowerPC/i', $agent))
    {
      $os = 'PowerPC';
    }
    else if (preg_match('/AIX/i', $agent))
    {
      $os = 'AIX';
    }
    else if (preg_match('/HPUX/i', $agent))
    {
      $os = 'HPUX';
    }
    else if (preg_match('/NetBSD/i', $agent))
    {
      $os = 'NetBSD';
    }
    else if (preg_match('/BSD/i', $agent))
    {
      $os = 'BSD';
    }
    else if (preg_match('/OSF1/i', $agent))
    {
      $os = 'OSF1';
    }
    else if (preg_match('/IRIX/i', $agent))
    {
      $os = 'IRIX';
    }
    else if (preg_match('/FreeBSD/i', $agent))
    {
      $os = 'FreeBSD';
    }
    else if (preg_match('/teleport/i', $agent))
    {
      $os = 'teleport';
    }
    else if (preg_match('/flashget/i', $agent))
    {
      $os = 'flashget';
    }
    else if (preg_match('/webzip/i', $agent))
    {
      $os = 'webzip';
    }
    else if (preg_match('/offline/i', $agent))
    {
      $os = 'offline';
    }
    else
    {
      $os = '未知操作';
    }
    return ($os."系统");
}
//判断是手机登录还是电脑登录
function ismobile() 
{
    // 如果有HTTP_X_WAP_PROFILE则一定是移动设备
    if (isset ($_SERVER['HTTP_X_WAP_PROFILE']))
        return "手机";
    //此条摘自TPM智能切换模板引擎，适合TPM开发
    if(isset ($_SERVER['HTTP_CLIENT']) &&'PhoneClient'==$_SERVER['HTTP_CLIENT'])
        return "手机";
    //如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息
    if (isset ($_SERVER['HTTP_VIA']))
        //找不到为flase,否则为true
        return stristr($_SERVER['HTTP_VIA'], 'wap') ? true : false;
    //判断手机发送的客户端标志,兼容性有待提高
    if (isset ($_SERVER['HTTP_USER_AGENT'])) {
        $clientkeywords = array(
            'nokia','sony','ericsson','mot','samsung','htc','sgh','lg','sharp','sie-','philips','panasonic','alcatel','lenovo','iphone','ipod','blackberry','meizu','android','netfront','symbian','ucweb','windowsce','palm','operamini','operamobi','openwave','nexusone','cldc','midp','wap','mobile'
        );
        //从HTTP_USER_AGENT中查找手机浏览器的关键字
        if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT']))) {
            return "手机";
        }
    }
    //协议法，因为有可能不准确，放到最后判断
    if (isset ($_SERVER['HTTP_ACCEPT'])) {
        // 如果只支持wml并且不支持html那一定是移动设备
        // 如果支持wml和html但是wml在html之前则是移动设备
        if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html')))) {
            return "手机";
        }
    }
    return "电脑";
}
/**
 * 管理员登录日志
 * @param $log_url 操作URL
 * @param $log_info 记录信息
 */
function adminLog($log_info)
{
    $admin_id = \think\facade\Session::get('admin_id') ? \think\facade\Session::get('admin_id') : 'null';
    $admin_id = !empty($admin_id) ? $admin_id : $admin_id;
    $add['admin_id'] = $admin_id;
    $add['log_info'] = $log_info;//操作类型
    $add['log_ip'] = clientIP();//客户端ip
    $add['log_browserType'] = GetBrowser();//浏览器
    $add['log_device'] = ismobile();//设备
    $add['log_osName'] = GetOs();//设备类型
    $add['log_time'] = getTime();//日记时间
    think\facade\Db::name('admin_log')->save($add);
}
/**
 * 过滤XSS攻击
 */
function remove_xss($val) 
{
    $val = preg_replace('/([\x00-\x08,\x0b-\x0c,\x0e-\x19])/', '', $val);
    $search = 'abcdefghijklmnopqrstuvwxyz';
    $search .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $search .= '1234567890!@#$%^&*()';
    $search .= '~`";:?+/={}[]-_|\'\\';
    for ($i = 0; $i < strlen($search); $i++) {
        $val = preg_replace('/(&#[xX]0{0,8}'.dechex(ord($search[$i])).';?)/i', $search[$i], $val);
        $val = preg_replace('/(&#0{0,8}'.ord($search[$i]).';?)/', $search[$i], $val);
    }
    $ra1 = array('javascript', 'vbscript', 'expression', 'applet', 'meta', 'xml', 'blink', 'link', 'style', 'script', 'embed', 'object', 'iframe', 'frame', 'frameset', 'ilayer', 'layer', 'bgsound', 'title', 'base');
    $ra2 = array('onabort', 'onactivate', 'onafterprint', 'onafterupdate', 'onbeforeactivate', 'onbeforecopy', 'onbeforecut', 'onbeforedeactivate', 'onbeforeeditfocus', 'onbeforepaste', 'onbeforeprint', 'onbeforeunload', 'onbeforeupdate', 'onblur', 'onbounce', 'oncellchange', 'onchange', 'onclick', 'oncontextmenu', 'oncontrolselect', 'oncopy', 'oncut', 'ondataavailable', 'ondatasetchanged', 'ondatasetcomplete', 'ondblclick', 'ondeactivate', 'ondrag', 'ondragend', 'ondragenter', 'ondragleave', 'ondragover', 'ondragstart', 'ondrop', 'onerror', 'onerrorupdate', 'onfilterchange', 'onfinish', 'onfocus', 'onfocusin', 'onfocusout', 'onhelp', 'onkeydown', 'onkeypress', 'onkeyup', 'onlayoutcomplete', 'onload', 'onlosecapture', 'onmousedown', 'onmouseenter', 'onmouseleave', 'onmousemove', 'onmouseout', 'onmouseover', 'onmouseup', 'onmousewheel', 'onmove', 'onmoveend', 'onmovestart', 'onpaste', 'onpropertychange', 'onreadystatechange', 'onreset', 'onresize', 'onresizeend', 'onresizestart', 'onrowenter', 'onrowexit', 'onrowsdelete', 'onrowsinserted', 'onscroll', 'onselect', 'onselectionchange', 'onselectstart', 'onstart', 'onstop', 'onsubmit', 'onunload');
    $ra = array_merge($ra1, $ra2);
    $found = true;
    while ($found == true) {
        $val_before = $val;
        for ($i = 0; $i < sizeof($ra); $i++) {
            $pattern = '/';
            for ($j = 0; $j < strlen($ra[$i]); $j++) {
                if ($j > 0) {
                    $pattern .= '(';
                    $pattern .= '(&#[xX]0{0,8}([9ab]);)';
                    $pattern .= '|';
                    $pattern .= '|(&#0{0,8}([9|10|13]);)';
                    $pattern .= ')*';
                }
                $pattern .= $ra[$i][$j];
            }
            $pattern .= '/i';
            $replacement = substr($ra[$i], 0, 2)."<x>".substr($ra[$i], 2);
            $val = preg_replace($pattern, $replacement, $val);
            if ($val_before == $val) {
                $found = false;
            }
        }
    }
    return $val;
}
//允许发布文档的栏目列表
function allow_release_arctype($selected = 0, $allow_release_channel = array(), $selectform = true)
{
    $where = [];
    $where[] = array('c.is_del','=',0); //回收站功能

    //权限控制
    $admin_info = \think\facade\session::get('admin_info');
    if (intval($admin_info['role_id']) > 1) {
        $auth_role_info = $admin_info['auth_role_info'];
        if(! empty($auth_role_info)){
            if(! empty($auth_role_info['permission']['arctype'])){
                $where[] = array('c.id','IN',$auth_role_info['permission']['arctype']);
            }
        }
    }
	
    if (!is_array($selected)) {
        $selected = [$selected];
    }
    $cacheKey = json_encode($selected).json_encode($allow_release_channel).$selectform.json_encode($where);
    $select_html = \think\facade\Cache::get($cacheKey);//获取缓存
    if (empty($select_html) || false == $selectform) {
        $pcfglobal = get_global();
        //允许发布文档的模型
        $allow_release_channel = !empty($allow_release_channel) ? $allow_release_channel : $pcfglobal['allow_release_channel'];
        //所有栏目分类
        $arctype_max_level = intval($pcfglobal['arctype_max_level']);// 栏目最多级别
        $where[] = array('c.status','=',1);
        $fields = "c.id, c.parent_id, c.current_channel, c.typename, c.grade, count(s.id) as has_children, '' as children";
        $res = \think\facade\db::name('arctype')
            ->field($fields)
            ->alias('c')
            ->join('arctype s','s.parent_id = c.id','LEFT')
            ->where($where)
            ->group('c.id')
            ->order('c.parent_id asc, c.sort_order asc, c.id')
            ->select()->toArray();
        \think\facade\Cache::tag('arctype')->set($cacheKey, $res, PCFCMS_CACHE_TIME);
        if (empty($res)) {
            return '';
        }
        //过滤掉第三级栏目属于不允许发布的模型下
        foreach ($res as $key => $val) {
            if ($val['grade'] == ($arctype_max_level - 1) && !in_array($val['current_channel'], $allow_release_channel)) {
                unset($res[$key]);
            }
        }
        //所有栏目列表进行层次归类
        $arr = group_same_key($res, 'parent_id');
        for ($i=0; $i < $arctype_max_level; $i++) {
            foreach ($arr as $key => $val) {
                foreach ($arr[$key] as $key2 => $val2) {
                    if (!isset($arr[$val2['id']])) {
                        $arr[$key][$key2]['has_children'] = 0;
                        continue;
                    }
                    $val2['children'] = $arr[$val2['id']];
                    $arr[$key][$key2] = $val2;
                }
            }
        }
        //过滤掉第二级不包含允许发布模型的栏目
        $nowArr = $arr[0];
        foreach ($nowArr as $key => $val) {
            if (!empty($nowArr[$key]['children'])) {
                foreach ($nowArr[$key]['children'] as $key2 => $val2) {
                    if (empty($val2['children']) && !in_array($val2['current_channel'], $allow_release_channel)) {
                        unset($nowArr[$key]['children'][$key2]);
                    }
                }
            }
            if (empty($nowArr[$key]['children']) && !in_array($nowArr[$key]['current_channel'], $allow_release_channel)) {
                unset($nowArr[$key]);
                continue;
            }
        }
        //组装成层级下拉列表框
        $select_html = '';
        if (false == $selectform) {
            $select_html = $nowArr;
        } else if (true == $selectform) {
            foreach ($nowArr AS $key => $val){
                $select_html .= '<option value="' . $val['id'] . '" data-grade="' . $val['grade'] . '" data-current_channel="' . $val['current_channel'] . '"';
                $select_html .= (in_array($val['id'], $selected)) ? ' selected="ture"' : '';
                if (!empty($allow_release_channel) && !in_array($val['current_channel'], $allow_release_channel)) {
                    $select_html .= ' disabled="true" style="background-color:#f5f5f5;"';
                }
                $select_html .= '>';
                if ($val['grade'] > 0){
                    $select_html .= str_repeat('&nbsp;', $val['grade'] * 4);
                }
                $select_html .= htmlspecialchars(addslashes($val['typename'])) . '</option>';
                if (empty($val['children'])) {
                    continue;
                }
                foreach ($nowArr[$key]['children'] as $key2 => $val2) {
                    $select_html .= '<option value="' . $val2['id'] . '" data-grade="' . $val2['grade'] . '" data-current_channel="' . $val2['current_channel'] . '"';
                    $select_html .= (in_array($val2['id'], $selected)) ? ' selected="ture"' : '';
                    if (!empty($allow_release_channel) && !in_array($val2['current_channel'], $allow_release_channel)) {
                        $select_html .= ' disabled="true" style="background-color:#f5f5f5;"';
                    }
                    $select_html .= '>';
                    if ($val2['grade'] > 0){
                        $select_html .= str_repeat('&nbsp;', $val2['grade'] * 4);
                    }
                    $select_html .= htmlspecialchars(addslashes($val2['typename'])) . '</option>';
                    if (empty($val2['children'])) {
                        continue;
                    }
                    foreach ($nowArr[$key]['children'][$key2]['children'] as $key3 => $val3) {
                        $select_html .= '<option value="' . $val3['id'] . '" data-grade="' . $val3['grade'] . '" data-current_channel="' . $val3['current_channel'] . '"';
                        $select_html .= (in_array($val3['id'], $selected)) ? ' selected="ture"' : '';
                        if (!empty($allow_release_channel) && !in_array($val3['current_channel'], $allow_release_channel)) {
                            $select_html .= ' disabled="true" style="background-color:#f5f5f5;"';
                        }
                        $select_html .= '>';
                        if ($val3['grade'] > 0){
                            $select_html .= str_repeat('&nbsp;', $val3['grade'] * 4);
                        }
                        $select_html .= htmlspecialchars(addslashes($val3['typename'])) . '</option>';
                    }
                }
            }
            \think\facade\Cache::tag('admin_archives_release')->set($cacheKey, $select_html);   
        }
    }
    return $select_html;
}
//获取全部的模型
function getChanneltypeList()
{
    $result = \think\facade\Cache::get('admin_channeltype_list_logic');
    if ($result == false){
        $result = \think\facade\db::name('channel_type')->column('*', 'id');
        \think\facade\Cache::set('admin_channeltype_list_logic', $result , 86400);
    }
    return $result;
}
/**
 * 获取文章内容html中第一张图片地址
 * @param  string $html html代码
 * @return boolean
 */
function get_html_first_imgurl($html)
{
    $pattern = '~<img [^>]*[\s]?[\/]?[\s]?>~';
    preg_match_all($pattern, $html, $matches);//正则表达式把图片的整个都获取出来了
    $img_arr = $matches[0];//图片
    $first_img_url = "";
    if (!empty($img_arr)) {
        $first_img = $img_arr[0];
        $p="#src=('|\")(.*)('|\")#isU";//正则表达式
        preg_match_all ($p, $first_img, $img_val);
        if(isset($img_val[2][0])){
            $first_img_url = $img_val[2][0]; //获取第一张图片地址
        }
    }
    return $first_img_url;
}
/**
 * 过滤前后空格等多种字符
 * @param string $str 字符串
 * @param array $arr 特殊字符的数组集合
 * @return string
 */
function trim_space($str, $arr = array())
{
    if (empty($arr)) {
        $arr = array(' ', '　');
    }
    foreach ($arr as $key => $val) {
        $str = preg_replace('/(^'.$val.')|('.$val.'$)/', '', $str);
    }
    return $str;
}
/**
 * 在文档列表显示文档属性标识
 * @param array $archivesInfo 文档信息
 * @return string
 */
function showArchivesFlagStr($archivesInfo = [])
{
    $arr = [];
    if (!empty($archivesInfo['is_head'])) {
        $arr['is_head'] = [
            'small_name'   => '头条',
        ];
    }
    if (!empty($archivesInfo['is_recom'])) {
        $arr['is_recom'] = [
            'small_name'   => '推荐',
        ];
    }
    if (!empty($archivesInfo['is_special'])) {
        $arr['is_special'] = [
            'small_name'   => '特荐',
        ];
    }
    if (!empty($archivesInfo['is_b'])) {
        $arr['is_b'] = [
            'small_name'   => '加粗',
        ];
    }
    if (!empty($archivesInfo['is_litpic'])) {
        $arr['is_litpic'] = [
            'small_name'   => '图片',
        ];
    }
    if (!empty($archivesInfo['is_jump'])) {
        $arr['is_jump'] = [
            'small_name'   => '跳转',
        ];
    }
    return $arr;
}
/**
 * 字符串截取，支持中文和其他编码
 * @param string $str 需要转换的字符串
 * @param string $start 开始位置
 * @param string $length 截取长度
 * @param string $suffix 截断显示字符
 * @param string $charset 编码格式
 */
function msubstr($str='', $start=0, $length=NULL, $suffix=false, $charset="utf-8") 
{
    if(function_exists("mb_substr"))
        $slice = mb_substr($str, $start, $length, $charset);
    elseif(function_exists('iconv_substr')) {
        $slice = iconv_substr($str,$start,$length,$charset);
        if(false === $slice) {
            $slice = '';
        }
    }else{
        $re['utf-8']   = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
        $re['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
        $re['gbk']    = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
        $re['big5']   = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";
        preg_match_all($re[$charset], $str, $match);
        $slice = join("",array_slice($match[0], $start, $length));
    }
    $str_len = strlen($str); // 原字符串长度
    $slice_len = strlen($slice); // 截取字符串的长度
    if ($slice_len < $str_len) {
        $slice = $suffix ? $slice.'...' : $slice;
    }
    return $slice;
}
/**
 *  远程图片本地化
 *
 * @access    public
 * @param     string  $body  内容
 * @return    string
 */
function remote_to_local( $body = '' )
{
    $web_basehost = tpCache('web.web_basehost');
    $parse_arr = parse_url($web_basehost);
    $web_basehost = $parse_arr['scheme'].'://'.$parse_arr['host'];
    $basehost = request()->domain();
    $img_array = array();
    preg_match_all("/src=[\"|'|\s]([^\"|^\'|^\s]*?)/isU", $body, $img_array);
    $img_array = array_unique($img_array[1]);
    foreach ($img_array as $key => $val) {
        if(preg_match("/^http(s?):\/\/mmbiz.qpic.cn\/(.*)\?wx_fmt=(\w+)&/", $val) == 1){
            unset($img_array[$key]);
        }
    }
    $dirname = 'uploads/ueditor/'.date('Ymd/');
    //创建目录失败
    if(!file_exists($dirname) && !mkdir($dirname,0777,true)){
        return $body;
    }else if(!is_writeable($dirname)){
        return $body;
    }

    foreach($img_array as $key=>$value)
    {
        $imgUrl = trim($value);
        // 本站图片
        if(preg_match("#".$basehost."#i", $imgUrl))
        {
            continue;
        }
        // 根网址图片
        if($web_basehost != $basehost && preg_match("#".$web_basehost."#i", $imgUrl))
        {
            continue;
        }
        // 不是合法链接
        if(!preg_match("#^http(s?):\/\/#i", $imgUrl))
        {
            continue;
        }

        $heads = @get_headers($imgUrl, 1);

        // 获取请求头并检测死链
        if (empty($heads)) {
            continue;
        } else if(!(stristr($heads[0],"200") && stristr($heads[0],"OK"))){
            continue;
        }
        // 图片扩展名
        $fileType = substr($heads['Content-Type'], -4, 4);
        if(!preg_match("#\.(jpg|jpeg|gif|png|ico|bmp)#i", $fileType))
        {
            if($fileType=='image/gif')
            {
                $fileType = ".gif";
            }
            else if($fileType=='image/png')
            {
                $fileType = ".png";
            }
            else if($fileType=='image/x-icon')
            {
                $fileType = ".ico";
            }
            else if($fileType=='image/bmp')
            {
                $fileType = ".bmp";
            }
            else
            {
                $fileType = '.jpg';
            }
        }
        $fileType = strtolower($fileType);
        //格式验证(扩展名验证和Content-Type验证)，链接contentType是否正确
        $is_weixin_img = false;
        if(preg_match("/^http(s?):\/\/mmbiz.qpic.cn\/(.*)/", $imgUrl) != 1){
            $allowFiles = [".png", ".jpg", ".jpeg", ".gif", ".bmp", ".ico", ".webp"];
            if(!in_array($fileType,$allowFiles) || (isset($heads['Content-Type']) && !stristr($heads['Content-Type'],"image/"))){
                continue;
            }
        } else {
            $is_weixin_img = true;
        }

        //打开输出缓冲区并获取远程图片
        ob_start();
        $context = stream_context_create(
            array('http' => array(
                'follow_location' => false // don't follow redirects
            ))
        );
        readfile($imgUrl,false,$context);
        $img = ob_get_contents();
        ob_end_clean();
        preg_match("/[\/]([^\/]*)[\.]?[^\.\/]*$/",$imgUrl,$m);

        $file = [];
        $file['oriName'] = $m ? $m[1] : "";
        $file['filesize'] = strlen($img);
        $file['ext'] = $fileType;
        $file['name'] = \think\facade\Session::get('admin_id').'-'.dd2char(date("ymdHis").mt_rand(100,999)).$file['ext'];
        $file['fullName'] = $dirname.$file['name'];
        $fullName = $file['fullName'];

        //检查文件大小是否超出限制
        if($file['filesize'] >= 20480000){
            continue;
        }

        //移动文件
        if(!(file_put_contents($fullName, $img) && file_exists($fullName))){ //移动失败
            continue;
        }

        $fileurl = '/'.$file['fullName'];
        if ($is_weixin_img == true) {
            $fileurl .= "?";
        }
        print_water($fileurl);
        $body = str_replace($imgUrl, $fileurl, $body);
        // 添加水印
    }
    return $body;
}
/**
 *  清除非站内链接
 *
 * @access    public
 * @param     string  $body  内容
 * @param     array  $allow_urls  允许的超链接
 * @return    string
 */
function replace_links( $body, $allow_urls=array() )
{
    // 读取允许的超链接设置
    $host = request()->host(true);
    if (!empty($allow_urls)) {
        $allow_urls = array_merge([$host], $allow_urls);
    } else {
        $basic_body_allowurls = tpCache('basic.basic_body_allowurls');
        if (!empty($basic_body_allowurls)) {
            $allowurls = explode(PHP_EOL, $basic_body_allowurls);
            $allow_urls = array_merge([$host], $allowurls);
        } else {
            $allow_urls = [$host];
        }
    }

    $host_rule = join('|', $allow_urls);
    $host_rule = preg_replace("#[\n\r]#", '', $host_rule);
    $host_rule = str_replace('.', "\\.", $host_rule);
    $host_rule = str_replace('/', "\\/", $host_rule);
    $arr = '';
    preg_match_all("#<a([^>]*)>(.*)<\/a>#iU", $body, $arr);
    if( is_array($arr[0]) )
    {
        $rparr = array();
        $tgarr = array();
        foreach($arr[0] as $i=>$v)
        {
            if( $host_rule != '' && preg_match('#'.$host_rule.'#i', $arr[1][$i]) )
            {
                continue;
            } else {
                $rparr[] = $v;
                $tgarr[] = $arr[2][$i];
            }
        }
        if( !empty($rparr) )
        {
            $body = str_replace($rparr, $tgarr, $body);
        }
    }
    $arr = $rparr = $tgarr = '';
    return $body;
}
/**
 *  给图片增加水印
 *
 * @access    public
 * @param     string  $imgpath  不带子目录的图片路径
 * @return    string
 */
function print_water($imgpath = '')
{
    try {
        static $water = null;
        null === $water && $water = tpCache('water');
        if (empty($imgpath) || $water['is_mark'] != 1) {
            return $imgpath;
        }
        $imgpath = preg_replace('#^(/[/\w]+)?(/public/uploads/|/uploads/)#i', '$2', $imgpath); // 支持子目录
        $imgresource = ".".$imgpath;
        $image = \think\Image::open($imgresource);
        $return_data['mark_type'] = $water['mark_type'];
        if($image->width()>$water['mark_width'] && $image->height()>$water['mark_height']){
            if($water['mark_type'] == 'text'){
                $ttf = public_path().'public/common/font/hgzb.ttf';
                if (file_exists($ttf)) {
                    $size = $water['mark_txt_size'] ? $water['mark_txt_size'] : 30;
                    $color = $water['mark_txt_color'] ? $water['mark_txt_color'] : '#000000';
                    if (!preg_match('/^#[0-9a-fA-F]{6}$/', $color)) {
                        $color = '#000000';
                    }
                    $transparency = intval((100 - $water['mark_degree']) * (127/100));
                    $color.= dechex($transparency);
                    $image->open($imgresource)->text($water['mark_txt'], $ttf, $size, $color, $water['mark_sel'])->save($imgresource);
                    $return_data['mark_txt'] = $water['mark_txt'];
                }
            }else{
                $water['mark_img'] = preg_replace('#^(/[/\w]+)?(/public/uploads/|/uploads/)#i', '$2', $water['mark_img']); // 支持子目录
                $waterPath = public_path()."public".$water['mark_img'];
                $waterPath = str_replace("\/", "/", $waterPath);
                if (eyPreventShell($waterPath) && file_exists($waterPath)) {
                    $quality = $water['mark_quality'] ? $water['mark_quality'] : 80;
                    $waterTempPath = dirname($waterPath).'/temp_'.basename($waterPath);
                    $image->open($waterPath)->save($waterTempPath, null, $quality);
                    $image->open($imgresource)->water($waterTempPath, $water['mark_sel'], $water['mark_degree'])->save($imgresource);
                    @unlink($waterTempPath);
                }
            }
        }
    } catch (\Exception $e) {}
}
if (!function_exists('getDirFile')) 
{ 
    /**
     * 递归读取文件夹文件
     *
     * @param string $directory 目录路径
     * @param string $dir_name 显示的目录前缀路径
     * @param array $arr_file 是否删除空目录
     * @return boolean
     */
    function getDirFile($directory, $dir_name='', &$arr_file = array()) {
        if (!file_exists($directory) ) {
            return false;
        }
        $mydir = dir($directory);

        while($file = $mydir->read())
        {
            if((is_dir("$directory/$file")) AND ($file != ".") AND ($file != ".."))
            {
                if ($dir_name) {
                    getDirFile("$directory/$file", "$dir_name/$file", $arr_file);
                } else {
                    getDirFile("$directory/$file", "$file", $arr_file);
                }
            }
            else if(($file != ".") AND ($file != ".."))
            {
                if ($dir_name) {
                    $arr_file[] = "$dir_name/$file";
                } else {
                    $arr_file[] = "$file";
                }
            }
        }
        $mydir->close();
        return $arr_file;
    }
}

if (!function_exists('sitemap_all')) 
{
    /**
     * 生成全部引擎sitemap
     */
    function sitemap_all()
    {
        $globalConfig = tpCache('sitemap');
        if (!isset($globalConfig['sitemap_xml']) || empty($globalConfig['sitemap_xml'])) {
            return false;
        }
        $filename = WWW_ROOT."/sitemap.xml"; //生成到指定目录
        $site = new Sitemap();
        // 用于生成分类列表
        $map = array(
            'status'    => 1,
            'is_del'    => 0,
            'is_hidden' => 0,
            //'is_part'   => 0, //外部链接
        );
        $result_arctype = \think\facade\Db::name('arctype')->field("*, id AS loc, add_time AS lastmod, 'hourly' AS changefreq, '0.8' AS priority")
            ->where($map)
            ->order('sort_order asc, id asc')
            ->select()->toArray();

        if(isset($result_arctype) && !empty($result_arctype))
        {
            $result_arctype = array_combine(array_column($result_arctype, 'id'), $result_arctype);
            // 用于生成文章详情链接
            $pcfglobal = get_global();
            $map = [];
            $map[] = ['channel','IN',$pcfglobal['allow_release_channel']];
            $map[] = ['arcrank','>=',0];
            $map[] = ['status','=',1];
            $map[] = ['is_del','=',0];
            //$map[] = ['is_jump','=',0];
            if (!isset($globalConfig['sitemap_archives_num']) || $globalConfig['sitemap_archives_num'] == '') {
                $sitemap_archives_num = 100;
            } else {
                $sitemap_archives_num = intval($globalConfig['sitemap_archives_num']);
            }
            $field = "aid,channel, is_jump, jumplinks, add_time, update_time, typeid, aid AS loc, add_time AS lastmod, 'daily' AS changefreq, '0.5' AS priority";
            $result1_archives = \think\facade\Db::name('archives')->field($field)
                ->where($map)
                ->order('aid desc')
                ->limit($sitemap_archives_num)
                ->select()->toArray();

            // 首页
            $url = \think\facade\request::domain();
            $site->AddItem($url,0);
            // 所有栏目
            foreach ($result_arctype as $sub){
                if ($sub['is_part'] == 1) {
                    $row1 = $sub['typelink'];
                } else {
                    $row1 = get_typeurl($sub, false);
                }
                $row1 = str_replace('&amp;', '&', $row1);
                $row1 = str_replace('&', '&amp;', $row1);
                try {
                    $site->AddItem($url.$row1, 1);
                } catch (\Exception $e) {}
            }
            if(!empty($result1_archives)){
                $result_archives  = array();
                // 所有文档内容
                foreach ($result1_archives as $key => $val) 
                {
                    if (is_array($val) && isset($result_arctype[$val['typeid']])) {
                        $result_archives[$key] = array_merge($result_arctype[$val['typeid']], $val);
                    }
                }            
            }
            if(isset($result_archives) && !empty($result_archives)){
                foreach ($result_archives as $key => $row) {
                    if ($row['is_jump'] == 1) {
                        $row1 = $row['jumplinks'];
                    } else {
                        $row1 = get_arcurl($row, false);
                    }
                    $row1 = str_replace('&amp;', '&', $row1);
                    $row1 = str_replace('&', '&amp;', $row1);
                    try {
                        $site->AddItem($url.$row1, 2);
                    } catch (\Exception $e) {}
                } 
                          
            }
            $site->SaveToFile($filename);     
        }

    }

}


