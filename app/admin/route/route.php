<?php
/***********************************************************
 * 路由
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
use think\facade\Route;
use think\facade\Db;
use think\facade\Cache;

$seo = tpCache('seo');
$seo_pseudo = $seo['seo_pseudo'];
$seo_rewrite_format = $seo['seo_rewrite_format'];
if ($seo_pseudo == 3){
    $lang_rewrite_str = '';
     // 精简伪静态
    if ($seo_rewrite_format == 1) {
        // 列表页
        Route::get('<tid>$', 'Lists/index');
        // 内容页
        Route::get('View/<dirname>/<aid>$', 'View/index');
        // 单页
        Route::get('single/<tid>$', 'Single/lists');
    }else{
        // 文章模型
        Route::get('article/<tid>$', 'Article/lists');
        Route::get('article/<dirname>/<aid>$', 'Article/view');
        // 单页
        Route::get('<tid>$', 'Single/lists');
        // 自定义模型
        $cacheKey = "application_route_channeltype";
        $channeltype_row = Cache::get($cacheKey);
        if (empty($channeltype_row)) {
            $channeltype_row = Db::name('channel_type')->field('nid,ctl_name')->where('ifsystem', 0)->select()->toArray();
            Cache::tag('channeltype')->set($cacheKey, $channeltype_row, PCFCMS_CACHE_TIME); 
        }
        foreach ($channeltype_row as $value) {
            // 自定义模型列表
            Route::get($value['nid'].'/<tid>$', $value['ctl_name'].'/lists');
            // 自定义模型内容
            Route::get($value['nid'].'/<dirname>/<aid>$', $value['ctl_name'].'/view');
        }
    }
}
