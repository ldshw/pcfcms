<?php
/***********************************************************
 * 基础控制器
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\controller;

use think\facade\Session;
use think\facade\Cache;
use think\facade\Request;
use app\BaseController;
use think\facade\Cookie;
class Base extends BaseController
{
    protected function initialize()
    {
        parent::initialize();

        //查看授权状态
        $is_gzpcf_authortoken = !empty(tpCache('web.web_is_authortoken')) ? tpCache('web.web_is_authortoken') : 0;
        $this->assign('is_gzpcf_authortoken', $is_gzpcf_authortoken);

        //获取当前用户是否登录
        $admin_id = Session::get('admin_id');
        if(empty($admin_id)){
             $this->redirect(url('/login/index')->suffix(false)->domain(true)->build());
        }
        $pcfglobal = get_global();
        //过滤不需要登录的操作
        $filter_login_action = [
            'Login/index',      //登录页面
            'Login/captcha',    //登录验证码
            'Login/logout',     //退出登录
        ];
        //过滤不需要登陆的行为
        $ctl_act = Request::controller().'/'.Request::action();
        $ctl_all = Request::controller().'/*';
        if (!in_array($ctl_act, $filter_login_action) || !in_array($ctl_all, $filter_login_action)) {
            $admin_login_expire = session::get('admin_login_expire'); // 登录有效期
            if (!empty($admin_id) && ((getTime() - intval($admin_login_expire)) < config('params.login_expire'))) {
                session::set('admin_login_expire', getTime()); // 登录有效期
            }else{
                session_unset();
                session::clear();
                $domain = url('/login/index')->suffix(false)->domain(true)->build();
                $this->redirect($domain); 
            }
        }
    }

    //错误提醒页面
    public function errorNotice($msg = '操作失败',$backUrl = false,$wait = 3 ,$type = false)
    {
        $this->assign(compact('msg','backUrl','wait','type'));
        return $this->fetch('public/message');
    }

    //文档错误提醒页面
    public function contentNotice($msg = '操作失败',$backUrl = false,$wait = 3 ,$type = false)
    {
        $this->assign(compact('msg','backUrl','wait','type'));
        return $this->fetch('public/conmessage');
    }


}
