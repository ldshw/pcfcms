<?php
/***********************************************************
 * 广告管理
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\controller\channel;
use think\facade\Db;
use think\facade\Request;
use app\admin\controller\Base;
use app\admin\model\Ad;
class Adposition extends Base
{

    public $popedom = '';
    public function initialize() {
        parent::initialize();
        $ctl_act = Request::controller().'/index';
        $this->popedom = appfile_popedom($ctl_act);
    }

    //列表
    public function index(){ 
        //防止php超时
        function_exists('set_time_limit') && set_time_limit(0);
        //验证权限
        if(!$this->popedom["list"]){
            return $this->errorNotice(config('params.auth_msg.list'),true,3,false);
        }
        if (Request::isAjax()) {
            $Admodel = new Ad();
            return $Admodel->tableData(input('param.'));
        }
        return $this->fetch();
    }
    
    //添加
    public function add(){
        //防止php超时
        function_exists('set_time_limit') && set_time_limit(0);
        if(input('get.ajax/d') == 1){
            $result['status'] = true;  
            return $result;
        }else{
            if (Request::isPost()) {
                //验证权限
                if(!$this->popedom["add"]){
                    if(config('params.auth_msg.test')){
                        $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                        return $result;
                    }else{
                        $result = ['status' => false, 'msg' => config('params.auth_msg.add')];
                        return $result;                    
                    }
                }
                $Admodel = new Ad();
                return $Admodel->toAdd(input('param.'));
            }
            return $this->fetch();            
        }
    }

    //编辑
    public function edit(){
        //防止php超时
        function_exists('set_time_limit') && set_time_limit(0);
        $result = ['status' => false,'msg' => '失败','data' => ''];
        $channelinfo = Db::name('ad_position')->where(['id' => input('get.id/d')])->find();
        if(input('get.ajax/d') == 1){
            if (!$channelinfo) {
                $result['status'] = false;
                $result['msg']    = '广告不存在，请联系管理员！';
                return $result;
            }else{
              $result['status'] = true;  
              return $result;
            }
        }else{
            $assign_data = array();
            if (Request::isPost()) {
                //验证权限
                if(!$this->popedom["modify"]){
                    if(config('params.auth_msg.test')){
                        $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                        return $result;
                    }else{
                        $result = ['status' => false, 'msg' => config('params.auth_msg.modify')];
                        return $result;                    
                    }
                }
                $Admodel = new Ad();
                return $Admodel->toAdd(input('param.'));
            }
            $assign_data['field'] = $channelinfo;
            //广告
            $ad_data = Db::name('ad')->where('pid',$channelinfo['id'])->order('sort_order asc')->select()->toArray();
            $assign_data['ad_data'] = $ad_data;
            $this->assign($assign_data);
            return $this->fetch();
        }
    }

    //删除广告图片
    public function del_imgupload()
    {
        $id_arr = input('post.del_id/d');
        $id_arr = eyIntval($id_arr);
        if(Request::isPost() && !empty($id_arr)){
            //验证权限
            if(!$this->popedom["delete"]){
                return $this->errorNotice(config('params.auth_msg.delete'),true,3,false);
            }
            $r = Db::name('ad')->where(['id' => ['IN', $id_arr]])->delete();
        }
    }

    //删除
    public function del(){
        if (Request::isAjax()) {
            //验证权限
            if(!$this->popedom["delete"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.delete')];
                    return $result;                    
                }
            }
            $id_arr = input('get.id/d');
            $ad_count = Db::name('ad')->where('pid','=',$id_arr)->count();
            if ($ad_count > 0){
                $result = ['status' => false, 'msg' => '该位置下有广告，不允许删除，请先删除该位置下的广告'];
                return $result;  
            }
            if (Db::name('ad_position')->where('id','=',$id_arr)->delete()) {
                $result = ['status' => true, 'msg' => '删除成功'];
                return $result; 
            } else {
                $result = ['status' => false, 'msg' => '删除失败'];
                return $result; 
            }
            return $result;
        }       
    }

}
