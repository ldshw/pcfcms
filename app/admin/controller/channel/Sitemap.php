<?php
/***********************************************************
 * 站点地图
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\controller\channel;
use app\admin\controller\Base;
use think\facade\Request;
use think\facade\Db;
use think\facade\Cache;
class Sitemap extends Base
{

    public $popedom = '';
    public function initialize() {
        parent::initialize();
        $ctl_act = Request::controller().'/index';
        $this->popedom = appfile_popedom($ctl_act);
    }

    // 配置入口
    public function index()
    {
        if(!$this->popedom["list"]){
            return $this->errorNotice(config('params.auth_msg.list'),true,3,false);
        } 
        if (Request::isPost()) {
            if(!$this->popedom["modify"]){
                if(config('params.auth_msg.test')){
                    $result = ['status' => false, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['status' => false, 'msg' => config('params.auth_msg.modify')];
                    return $result;                    
                }
            }
            $param = input('param.');
            $param['sitemap_auto'] = isset($param['sitemap_auto']) ? $param['sitemap_auto'] : 0;
            $param['sitemap_xml'] = isset($param['sitemap_xml']) ? $param['sitemap_xml'] : 0;
            $param['sitemap_archives_num'] = isset($param['sitemap_archives_num']) ? intval($param['sitemap_archives_num']) : 100;
            tpCache('sitemap',$param);
            Cache::clear();//清除数据缓存文件
            $admin_temp = glob(root_path() . 'runtime/admin/temp/'. '*.php');//清除后台临时文件缓存
            array_map('unlink', $admin_temp);
            // 生成sitemap
            sitemap_all();
            // 百度推送
            $bdtsdz = Request::Domain()."/sitemap.xml";
            BaiduPush($bdtsdz);
            $result = ['status' => true, 'msg' => '设置成功'];
            return $result;   
        }
        $config = tpCache("sitemap");
        $this->assign('config',$config);//当前配置项
        return $this->fetch();
    }

}
