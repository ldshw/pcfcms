<?php
/***********************************************************
 * 模板设置
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
return [
    // 模板后缀
    'view_suffix'  => 'html',
    // 模板路径
    'view_path'    => app_path('view'),
    // 视图输出字符串内容替换
    'tpl_replace_string'       => [
        '{__PUBLIC_PATH}' =>  '',              //public 目录
        '{__STATIC_PATH}' =>  '/static',       //全局静态目录
        '{__ADMIN_PATH}'  =>  '/admin',        //后台目录
    ]
];
