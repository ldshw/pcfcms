<?php


return [

    //核心字符串
    'service_pcf' => "aHR0cDovL3VwZGF0ZS5wY2ZjbXMuY29tLw==",
    'service_pcf_token' => "cGNmY21z",

    // 网络图片扩展名
    'image_ext' => 'jpg,jpeg,gif,bmp,ico,png,webp',

    // 登录有效期
    'login_expire' => 86400,

    'web_exception' => 0, // 关闭php所有错误

    //权限提示
    'auth_msg' => [
        'test'      => false,
        'pcfcms'    => "演示站点，禁止操作",
        'list'      => "您没有查看权限", 
        'add'       => "您没有添加权限",
        'modify'    => "您没有修改权限",
        'delete'    => "您没有删除权限",
        'status'    => "您没有修改状态权限",
        'userinfo'  => "您没有权限",
        'databack'  => "您没有权限",
        'export'    => "您没有备份权限",
        'optimze'   => "您没有优化权限",
        'repair'    => "您没有修复权限",
        'import'    => "您没有恢复权限",
        'down'      => "您没有下载权限",
    ],

];
